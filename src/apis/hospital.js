export default {
    registerAppointment: (data) => window.axios.post('create-appointment/', data),
    registerContactInfo: (data) => window.axios.post('client-contact-us/add-contact-us-request/', data),
    getDepartments: (data) => window.axios.post('department/get-appointment-department-list/', data),
}